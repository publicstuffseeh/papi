# papi

papi: papi is another programming interface

This artificial being (aka agent or ai) does not implement anything by itself, it rather utilizes the internet and or humans to accomplish the (optional self choosen) goal.

The first test will be to get/create "Randy" a non pseudo-random (chaotic) number generator.


Implementierungsideen:
0. User wählt (später Randy): zu erlerndende Fähigkeit (API-Befehl)
1. Such API die so klingt, als könne Sie diese Fähigkeit (z.B. Internet, Github, usw.)
2. Erlange Fähigkeit
  * download
  * extract
  * parse howto install
  * build (./configure & make & make install [am besten als dll]
3. Rufe Sie auf (d.h. mache Testcalls)
  * List cmds
  * Init(s)
  * main
4. Falls Erfolg:
  * commit
  * packen in CoreListe
  * goto 0
Else
  * Retry 1-3


Idee für RANDY:

Summe aus ALLEN Zahlen von (sowohl IPs als auch Dauer)
C:\Users\seeh>tracert -4 google.de

Routenverfolgung zu google.de [172.217.21.195]
über maximal 30 Hops:

  1    <1 ms    <1 ms    <1 ms  192.168.35.254
  2    17 ms    28 ms    17 ms  dslb-092-075-000-001.092.075.pools.vodafone-ip.de [92.75.0.1]
  3    88 ms    84 ms    88 ms  88.79.8.48
  4   107 ms   102 ms    91 ms  92.79.214.174
  5   107 ms   101 ms     *     145.254.2.179
  6    98 ms   104 ms   102 ms  72.14.213.64
  7   106 ms   116 ms    97 ms  108.170.251.193
  8    98 ms   103 ms   106 ms  108.170.235.245
  9   107 ms    99 ms    96 ms  fra16s12-in-f195.1e100.net [172.217.21.195]
