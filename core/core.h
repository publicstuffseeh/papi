#pragma once
#ifdef _WIN64
  #include <windows.h>			// for LPWSTR
#endif
#include <winnt.h>
#include <iostream>
#include <string>

const char * downloadDLL = "download.dll";

typedef int(__cdecl *MYPROC)(std::string, std::string);
MYPROC ProcAdd;
HINSTANCE hinstLib;

// von https://stackoverflow.com/questions/6693010/how-do-i-use-multibytetowidechar
LPWSTR ToWidechar(std::string input) {
	int  length = MultiByteToWideChar(CP_ACP, 0, input.c_str(), -1, NULL, 0);
	LPWSTR output = new WCHAR[length];
	MultiByteToWideChar(CP_ACP, 0, input.c_str(), -1, output, length);
	return output;
}

std::string ToUtf8String(LPWSTR input) {
	size_t wcsChars = wcslen(input);
	int sizeRequired = WideCharToMultiByte(CP_UTF8, 0, input, -1, NULL, 0 , NULL, NULL);
	LPSTR output = new CHAR[sizeRequired];
	WideCharToMultiByte(CP_UTF8, 0, input, -1, output, sizeRequired, NULL, NULL);
	// gegebenfalls delete[] output;
	std::string retStr = output;
	return retStr;
	// printf("Bytes required for UTF8 encoding (excluding NUL terminator): %u\n",
}

// Hier Routine, die allgemein DLLs l�dt
// calling the dlls is from: https://docs.microsoft.com/de-de/windows/win32/dlls/using-run-time-dynamic-linking?redirectedfrom=MSDN
int ladeDLL(const char* dllName, const char* Funktion) {
	// Get a handle to the DLL module.
	//hinstLib = LoadLibrary(TEXT( dllName));
	hinstLib = LoadLibraryA(dllName);

	// If the handle is valid, try to get the function address.
	if (hinstLib != NULL) {
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, Funktion);
		// If the function address is valid, call the function.
		if (NULL != ProcAdd)
		{
//			std::cout << "OK soweit";
			return 0;							// Fehlercode 0 = gut
		}
		else {
			std::cout << "Fehler: ProcAdd = NULL!";
			return 1;
		}
	}
	else {
		std::cout << "Fehler konnte die DLL " << dllName << " nicht laden";
		return 1;
	}
}

int downloadFile(std::string url, std::string fileName) {
	//LPWSTR pUrl = ToWidechar(url);
	//LPWSTR pFile = ToWidechar(fileName);
	std::cout << "Going to url: " + url + "\n\tSaveing content in " + fileName + "\n";

	if (!ladeDLL(downloadDLL, "download")) {
		(ProcAdd)(url, fileName);				// <- hier wird der Download mit dem Function-Pointer ausgef�hrt.
	}
	if (hinstLib != NULL) {
		FreeLibrary(hinstLib);
		hinstLib = NULL;
	}
	return 0;
}