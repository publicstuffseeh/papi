// download.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

// copy-paste from: http://www.cplusplus.com/forum/windows/109799/ [done manually the job of papi]
// #pragma comment(lib, "wininet.lib") //remove if not using VC++.
#include "download.h"
#include "../core.h"
using namespace std;


int download(string url, string fileName) {
	HINTERNET connect = InternetOpenA("MyBrowser", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);

	if (!connect) {
		cout << "Connection Failed or Syntax error";
		return 0;
	}

	// printf("adresse: %ws", url);

	HINTERNET OpenAddress = InternetOpenUrl(connect, ToWidechar(url), NULL, 0, INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_KEEP_CONNECTION, 0);

	if (!OpenAddress)
	{
		DWORD ErrorNum = GetLastError();
		cout << "Failed to open URL \nError No: " << ErrorNum;
		InternetCloseHandle(connect);
		return 0;
	}

	char DataReceived[TRANSFERBYTES];
	memset(&DataReceived[0], 0, sizeof(DataReceived));
	DWORD NumberOfBytesRead = 0;
	ofstream datei;
	datei.open(fileName, ios::out |ios::binary);
	while (InternetReadFile(OpenAddress, DataReceived, TRANSFERBYTES, &NumberOfBytesRead) && NumberOfBytesRead)
	{
		// write to outfile
		//datei << DataReceived;
		datei.write(DataReceived, NumberOfBytesRead);
	}

	InternetCloseHandle(OpenAddress);
	InternetCloseHandle(connect);
	datei.close();
//	cin.get();
	return 0;
}
