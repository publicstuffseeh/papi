#pragma once

// aus stdafx.h
#define WIN32_LEAN_AND_MEAN             // Selten verwendete Komponenten aus Windows-Headern ausschlie�en
// Windows-Headerdateien
#include <windows.h>
#include <string>

#ifdef DOWNLOAD_EXPORTS
#define DOWNLOAD_API __declspec(dllexport)
#else
#define DOWNLOAD_API __declspec(dllimport)
#endif

extern "C" DOWNLOAD_API int download(std::string url, std::string fileNAme);

#include<iostream>
#include<fstream>
#include<wininet.h>
#include<cstring>
#include<stdio.h>

// Meine Deklarationen
constexpr auto TRANSFERBYTES = 4096;