// mainEntry.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "mainEntry.h"
#include "../core.h"

using std::string;
using std::cout;


string getRandomWord() {
	string buffer, empty;
	if (ladeDLL(randyDLL, "getRandomWord")) {
		(ProcAdd)(buffer, empty);
	}
	if (hinstLib != NULL) {
		FreeLibrary(hinstLib);
		hinstLib = NULL;
	}
	return buffer;
}

int getRandomNumber(string url) {
	string empty;
	int number = 0;
	if (!ladeDLL(randyDLL, "getRandomNumberFromUrl")) {
 		number = (ProcAdd)(url, empty);
	}
	else {
		cout << "Error!";
		return 1;
	}
	if (hinstLib != NULL) {
		FreeLibrary(hinstLib);
		hinstLib = NULL;
	}
	return number;
}


int main(int argc, char *cmds[])
{
	string job;
	// die erstmal einfachste Vesion an Code zu kommen:
	string search = "https://github.com/search?q=";
	string fileName = "result.html";
    cout << "Hello World!\n";
	// go with the readme:
	// 1. pick up job / (or choose one if none is given)
	if (argc > 1) {					// remember: argv[0] = ProgramName
		std::cout << cmds[1];
		search += cmds[1];
	}
	else
	{	// use randy to get a job
		search += "random()";

		// Testweise zufällige Zahl wählen (statt zufälligem Befehl)
		// Ermittle mal eine zufällige Zahl:
		string url = "https://www.spiegel.de";
		cout << "And the lucky number is: " << getRandomNumber(url) << "\r\n";
	}
	// 2. Search extensions / internet for routine to get source code to complete the job(s) [s = future]
	search += " language:c++";
	downloadFile(search, fileName);

	// 3. Parse for Repos:



	return 0;
}
