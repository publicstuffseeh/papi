#pragma once

// Windows-Headerdateien
#define WIN32_LEAN_AND_MEAN             // Selten verwendete Komponenten aus Windows-Headern ausschlie�en
#include <windows.h>
#include <string>
#include <sstream>

#include <memory>						// Block f�r myExec von Stackoverflow
#include <stdexcept>
#include <array>

#ifdef RANDY_EXPORTS
#define RANDY_API __declspec(dllexport)
#else
#define RANDY_API __declspec(dllimport)
#endif

// Definiert die exportierten Funktionen f�r die DLL - Anwendung.
extern "C" RANDY_API int getRandomWord(std::string randomSource1, std::string randomSource2);
extern "C" RANDY_API int getRandomNumberFromUrl(std::string url, std::string empty);
extern "C" RANDY_API int getRandomNumberFromTraceroute(std::string);
