// randy.cpp
//

#include "randy.h"
#include "../core.h"

// Vars für den Zufall:
// std::string seite1 = "http://www.spiegel.de"; wird in mainEntry.cpp bereits verwendet ;)
std::string seite2 = "https://www.hessenschau.de/verkehr/index.html";
std::string seite3 = "http://www.wetter.com";

// Hilfsfunktion von https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po
std::string myExec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe) {
		throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	return result;
}


int getRandomNumberFromUrl(std::string url, std::string empty) {
	std::cout << "Getting simple random number from url file size" << "\n";
	std::string fileName = "nrResult.html";
	downloadFile(url, fileName);
	// http://forums.codeguru.com/showthread.php?372471-GetFileSize
	HANDLE hFile = CreateFileA(fileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	return GetFileSize(hFile, NULL);
}

long sumAllNumbersFromString(std::string str) {
	const char * tmpString = str.c_str();
	char * pEnd;
	long result = 0L;
	size_t i = 0;
	for (; i < strlen(tmpString); i++) {
		if (isdigit(static_cast<unsigned char>(tmpString[i]))) {
			result += strtol(&tmpString[i], &pEnd, 10);
		}
	}
	return result;
}

int getRandomNumberFromTraceroute(std::string url) {
	url = url.substr(7);
	std::string cmdStr = "tracert -w 10 -d " + url;
	const char * cmd = cmdStr.c_str();
	std::stringstream myStream(myExec(cmd));
	std::string zeile;
	// std::cout << "Ergebnis: " << result; // std::string result = myExec(cmd);
	long outputZahl = 0;
	while (getline(myStream, zeile, '\n')) {
		// std::cout << "zeile: " << zeile;
		outputZahl += sumAllNumbersFromString(zeile);
	}


	// system(cmd);
	return outputZahl;
}

int getRandomWord(std::string randomSource1, std::string randomSource2) {  // int length
	std::cout << "Hallo RandomWord: Merke das Ergebnis ist als Buchstabe 1*1M + Buchstabe 2*1k + Buchstabe 3 in dem Integer gespeichert.";
	std::cout << "Bsp: 86.101.098 = Seb";
	// Hole Länge von 3 Quellen (nicht randomSource [ das ist eher "web", "rndGen", "..." ])

	// idee: for (i=1:length) { teil_i = teil_i + ....; }
	int teil1 = getRandomNumberFromUrl(randomSource1, "") % 256;
	int teil2 = getRandomNumberFromTraceroute(randomSource2) % 256;
	int teil3 = 42;
	return teil1 * 1000000 + teil2 * 1000 + teil3;
}

