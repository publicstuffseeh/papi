// randyTest.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
/* Testet dir implementierten DLL-Funktionen */

#include <iostream>
#include "../randy/randy.h"
using namespace std;

int main() {
	cout << "Hier sind ein paar Zufallszahlen: ";
	string leer;
	string myUrl = "http://www.web.de";
	// cout << "getRandomNumberFromUrl(" + myUrl + ", leer): " << getRandomNumberFromUrl(myUrl, leer);
	cout << "getRandomNumberFromTraceroute(" + myUrl + "): " << getRandomNumberFromTraceroute(myUrl);
	// cout << "getRandomWord(" + myUrl + ", " + myUrl + "): " << getRandomWord(myUrl, myUrl);

	return 0;
}
